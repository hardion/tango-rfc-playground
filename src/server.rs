use std::convert::TryInto;
use zeromq::*;
use std::collections::HashMap;

pub struct Server {
    socket: zeromq::RepSocket,

    requests: HashMap<String, Box<dyn Fn(String) -> String>>,

}

impl Server {

    pub fn new() -> Server {
         let socket = zeromq::RepSocket::new();
         let requests = HashMap::new();

         Server { socket, requests }
    }

    pub async fn initialise(&mut self, address: &str) -> Result< (), Box<dyn std::error::Error>> {
         println!("Start server");
         self.socket.bind(address).await?;
         Ok(())
    } 

    pub fn add(&mut self, request: String, f: impl Fn(String) -> String + 'static) {
         self.requests.insert(request, Box::new(f));
    }

    pub async fn run(&mut self) -> Result<(), Box<dyn std::error::Error>> {
    
        loop {
            let req: String = self.socket.recv().await?.try_into()?;
            dbg!(&req);

            let repl: String;

            match self.requests.get(&req) {
                Some(closure) => repl = (*closure(req)).to_string(),
                _ => repl = String::from("DEVFAILED: INVALID REQUEST"),
            }
            self.socket.send(repl.into()).await?;
        }
    }
}

