use std::error::Error;
use zeromq::{Socket, SocketRecv, SocketSend};
use std::convert::TryFrom;

pub struct Requestor {
    socket: zeromq::ReqSocket,
}

impl Requestor {
    pub fn new() -> Requestor {
        let socket = zeromq::ReqSocket::new();
        Requestor { socket }
    }

    pub async fn initialise(&mut self, address: &str) -> Result<(), Box<dyn Error>> {
        self.socket
            .connect(address)
            .await
            .expect("Failed to connect");
        println!("Connected to server");
        Ok(())
    }
    
    pub async fn request(&mut self, req: &str) -> Result<String, Box<dyn Error>> {
        self.socket.send(req.into()).await?;
        let repl = self.socket.recv().await?;
        dbg!(&repl);
        Ok(String::try_from(repl)?)
    }
}


