Tango RFC Playground
====================

Tango RFC is project aiming at specifying in essence the Tango distributed control system by skipping the implementation details.

The best way to verify the Tango RFC is coding a naive but faithful example.

Contributing
------------
Come and join me on my journey, you will see, this is quite fun and sometimes dumb easy to learn the intern of Tango.

The most important contribution is your review of the Tango RFC. You can fork this project or get your own inspiration with another language... nothing matter more than your feedback. And create an issue/merge request in the Tango RFC project.


You have finished the game! Well there is a second story behind. Try different protocols and wires, and then start to define the next generation of Tango with your finding. Create an issue in the tango RFC project with the tag "v10" and follow the contribution guideline. 

How to run the examples
-----------------------
With Rust and Cargo installed this is easy as "cargo run --examples ..." i.e:

RFC 10 Request/Reply, run the server first
```sh
 cargo run --examples server
```

In another terminal, run the client
```sh
 cargo run --examples server
```

Actual RFC examples
-------------------

RFC 10 Request/Reply:
- [x] Execute a command with one argument
- [ ] Read an Attribute
- [ ] Write an Attribute
- [ ] Read multiple Attribute
...
