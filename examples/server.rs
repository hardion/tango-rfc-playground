use tangorfc::server::Server;

use std::error::Error;

#[async_std::main]
async fn main() -> Result< (), Box<dyn Error>> {

    let mut server = Server::new();
    server.initialise("tcp://127.0.0.1:5555").await?;

    // See the example::client file for the requestor counter part
    // 1st Case the client read an attribute "attr1"
    let reply1 = |request: String| -> String { String::from("Request was: ") + &request }; 
    server.add(String::from("5 CMD_INOUT sys/tg_test/1 Echo Hello world!"), reply1); 
   
    // Square command 
    let reply2 = |_request: String| -> String { String::from("10000")  }; 
    server.add(String::from("5 CMD_INOUT sys/tg_test/1 Square 100"), reply2); 

    server.run().await
}
