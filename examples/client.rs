use tangorfc::requestor::Requestor;

use std::error::Error;

#[async_std::main]
async fn main() -> Result< (), Box<dyn Error>> {
    let mut client = Requestor::new();
    client.initialise("tcp://127.0.0.1:5555").await?;

    // This is an example how to fulfill RFC 10 Request/Reply
    // with a very naive Character-based protocol content 
    //  and with the REQUEST/REPLY of ZeroMQ

    // A Message in Tango includes:
    // * the request type
    // * the version of the protocol
    let version = "5";
    let request_type: &str;

    // here seems the device name is needed but not yet in RFC10
    let device_name = "sys/tg_test/1";

    // Case COMMAND Execution 
    request_type = "CMD_INOUT";

    // 1. Simple Echo
    let command_name = "Echo";
    let argin = "Hello world!";
    
    // When
    let request = format!("{} {} {} {} {}", 
                         version,
                         request_type,
                         device_name,
                         command_name,
                         argin);
    let reply = client.request(&request).await?;
    dbg!(&reply);

    // 2. Square calculation
    let command_name = "Square";
    let argin = "100";
    
    // When
    let request = format!("{} {} {} {} {}", 
                         version,
                         request_type,
                         device_name,
                         command_name,
                         argin);
    let reply = client.request(&request).await?;
    dbg!(&reply);

    // Case with implicit reconnection

    Ok(())

}
